package stompframes;

public class WorkingStompFrameExamples {

  /**
   * This is just a few examples we might use as templates in the future. Right now, we are using StompMq to build frames.
   * See EventEndPointClass
   */

  String destination = "/app/application"; // Destination for broadcasting messages
  String message = "Hello, world!"; // Message to be sent

  String connectFrame = "CONNECT\n" +
      "\n" +
      "\u0000"; // Null character to terminate the frame

  String sendFrame = "SEND\n" +
      "destination:" + destination + "\n" +
      "\n" +
      "{\"text\":\"" + message + "\"}\n" +
      "\u0000"; // Null character to terminate the frame

  String subscribeFrame = "SUBSCRIBE\n" +
      "id:sub-0\n" +
      "destination:" + destination + "\n" +
      "\n" +
      "\u0000"; // Null character to terminate the frame
}
