import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import java.net.URI;
import java.util.concurrent.Future;
import org.testng.Assert;

/**
 * This class is just a different solution to EventEndPoint class with annotation instead of overrides.
 * Right now it is not used , however in the future it might come handy.
 */

@WebSocket
public class SocketEndPoint {
  private Session session;
  private String text;
  private final CountDownLatch closureLatch = new CountDownLatch(1);

  @OnWebSocketConnect
  public void onConnect(Session session) {

    this.session = session;
    System.out.println("Connected to:" + session.toString());
  }

  @OnWebSocketMessage
  public void onMessage(String message) {
    Assert.assertEquals(message, "Hello");
  }

  public void connect(String destUri) throws Exception {
    WebSocketClient client = new WebSocketClient();
    client.start();
    URI echoUri = new URI(destUri);
    ClientUpgradeRequest request = new ClientUpgradeRequest();
    Future<Session> future = client.connect(this, echoUri, request);
    this.session = future.get();
  }

  public void subscribe(String subscribeDest) {
    try {
      this.session.getRemote().sendString(StompFrameBuilder.buildSubscribeFrame(subscribeDest));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    System.out.println("Subscribed to destination: " + subscribeDest);
  }

  @OnWebSocketClose
  public void onWebSocketClose(int statusCode, String reason) {
    System.out.println(statusCode+reason);
    closureLatch.countDown();
  }

  public void sendMessage(String message) throws Exception {
    this.session.getRemote().sendString(message);
  }

  public void awaitClosure() throws InterruptedException {
    closureLatch.await();
  }

  public String getMessage() {
    return this.text;
  }

}