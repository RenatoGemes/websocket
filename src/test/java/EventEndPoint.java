import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import messagelistener.JettyMessageListener;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventEndPoint extends WebSocketAdapter {

  private final CountDownLatch closureLatch = new CountDownLatch(2);
  private Session session;
  private JettyMessageListener messageListener;

  private static final Logger logger = LoggerFactory.getLogger(EventEndPoint.class);

  @Override
  public void onWebSocketConnect(Session sess) {
    super.onWebSocketConnect(sess);
    this.session = sess;
    logger.info("Endpoint connected: " + sess.toString());
    logger.info("Session open: " + session.isOpen());
    logger.info("Listener active: " + String.valueOf(messageListener != null));
    closureLatch.countDown();
  }

  @Override
  public void onWebSocketText(String message) {
    if (session.isOpen() && messageListener != null) {
      messageListener.onMessage(message);
    } else {
      logger.error("Session is not open or messageListener is null. Message: {}", message);
    }
  }

  @Override
  public void onWebSocketClose(int statusCode, String reason) {
    super.onWebSocketClose(statusCode, reason);
    logger.info("Closed: " + "with status code: " + statusCode + " with reason: " + reason);
  }

  public void awaitClosure() throws InterruptedException {
    closureLatch.await();
  }

  public void subscribe(String subscribeDest) {
    try {
      this.session.getRemote().sendString(StompFrameBuilder.buildSubscribeFrame(subscribeDest));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    logger.info("Subscribed to destination: " + subscribeDest);

  }

  public void sendMessage(String destination, String message) {
    try {
      this.session.getRemote().sendString(StompFrameBuilder.buildSendFrame(destination, message));
      closureLatch.countDown();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    logger.info("Message sent to destination: " + destination);
  }


  public void setMessageListener(JettyMessageListener messageListener) {
    this.messageListener = messageListener;
  }

  public JettyMessageListener getMessageListener() {
    return this.messageListener;
  }

  public Session getSession() {
    return this.session;
  }
}
