package messagelistener;

import java.util.concurrent.CompletableFuture;

public class JettyMessageListener implements MessageListener{

  private String message;
  @Override
  public void onMessage(String message) {
    CompletableFuture.runAsync(() -> {
      this.message=message;
    });
  }
  public String getMessage() {
    return this.message;
  }
}
