package messagelistener;

public interface MessageListener {
  void onMessage(String message);
}