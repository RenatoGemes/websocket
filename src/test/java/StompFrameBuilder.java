import java.util.HashMap;
import java.util.Map;
import org.apache.activemq.transport.stomp.Stomp;

public class StompFrameBuilder {

  public static String buildFrame(String command, Map<String, String> headers, String body) {
    StringBuilder frame = new StringBuilder();
    // Append command
    frame.append(command).append("\n");
    // Append headers
    for (Map.Entry<String, String> entry : headers.entrySet()) {
      frame.append(entry.getKey()).append(":").append(entry.getValue()).append("\n");
    }
    // Append optional body
    if (body != null && !body.isEmpty()) {
      frame.append("\n").append("{\"text\":\"" + body + "\"}\n");
    }
    // Append null byte to indicate end of frame
    frame.append("\n\0");
    return frame.toString();
  }

  public static String buildSendFrame(String destination, String message) {
    Map<String, String> headers = new HashMap<>();
    headers.put(Stomp.Headers.Send.DESTINATION, destination);
    return buildFrame(Stomp.Commands.SEND, headers, message);
  }
  public static String buildSendFrame(String destination, String message,String user) {
    Map<String, String> headers = new HashMap<>();
    headers.put(Stomp.Headers.Send.DESTINATION, destination);
    headers.put(Stomp.Headers.Send.CORRELATION_ID, user);
    return buildFrame(Stomp.Commands.SEND, headers, message);
  }

  public static String buildConnectFrame(String username, String password) {
    Map<String, String> headers = new HashMap<>();
    headers.put(Stomp.Headers.Connect.LOGIN, username);
    headers.put(Stomp.Headers.Connect.PASSCODE, password);
    return buildFrame(Stomp.Commands.CONNECT, headers,null);
  }
  public static String buildSubscribeFrame( String destination) {
    Map<String, String> headers = new HashMap<>();
    headers.put(Stomp.Headers.Subscribe.DESTINATION, destination);
    headers.put(Stomp.Headers.Subscribe.ID, "1");
    return buildFrame(Stomp.Commands.SUBSCRIBE, headers,null);
  }
}
