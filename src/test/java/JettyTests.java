import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.concurrent.ExecutionException;
import messagelistener.JettyMessageListener;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import java.net.URI;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class JettyTests {
  private final String stompEndpointUrl = "ws://localhost:8081/ws";
  private final String destination = "/app/application"; // Destination for broadcasting messages

  private final String topicToSubscribe = "/all/messages"; // Destination for broadcasting messages
  private final String message = "Hello, world, 2024!"; // Message to be sent
  private static final Logger logger = LoggerFactory.getLogger(JettyTests.class);

  @Test
  public void stompTest() {

    try {
      WebSocketClient client = new WebSocketClient();
      client.start();
      URI uri = new URI(stompEndpointUrl);
      EventEndPoint socket = new EventEndPoint();
      socket.setMessageListener(new JettyMessageListener());
      ClientUpgradeRequest request = new ClientUpgradeRequest();
      // Attempt Connect
      client.connect(socket, uri).get();

      if (socket.getSession()!=null) {
        //subscribe to a topic
        socket.subscribe(topicToSubscribe);
        // Send the message
        socket.sendMessage(destination,message);
        // Close the WebSocket client
        socket.awaitClosure();
      } else {
        logger.error("Failed to establish WebSocket connection.");
      }
    } catch (Throwable t) {
      t.printStackTrace();
    }
  }

  @Test
  public void failingConnectTest() {
    try {
      WebSocketClient client = new WebSocketClient();
      client.start();
      URI uri = new URI(stompEndpointUrl);
      EventEndPoint socket = new EventEndPoint();
      socket.setMessageListener(new JettyMessageListener());
      ClientUpgradeRequest request = new ClientUpgradeRequest();

      // Attempt Connect
      Future<Session> fut = client.connect(socket, uri, request);
      // Wait for Connect
      Session session = fut.get();
      if (session != null) {
        fail("Failed to establish WebSocket connection.");
      }
    } catch (Throwable exception) {
      assertTrue(exception instanceof ExecutionException, "Expected exception was not instance of ExecutionException ");
      logger.error(String.valueOf(exception));
    }
  }

  @Test
  public void simpleWebSocketTest() throws Exception {
    WebSocketClient client = new WebSocketClient();
    client.start();
    URI uri = new URI(stompEndpointUrl);
    EventEndPoint socket = new EventEndPoint();
    socket.setMessageListener(new JettyMessageListener());
    ClientUpgradeRequest request = new ClientUpgradeRequest();

    // Attempt Connect
    Future<Session> fut = client.connect(socket, uri, request);
    Session session = fut.get();
    if (session != null) {
      // Send the message
      session.getRemote().sendString(message);
      logger.info("Message sent to : " + destination);
      socket.awaitClosure();
      // Close the WebSocket client
    } else {
      logger.error("Failed to establish WebSocket connection.");
    }
  }
    // Close session
  }