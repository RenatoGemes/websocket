
# Websocket/Stomp Server

The JavaScript Framework needs a running websocket/stomp server. A custom-made spring based server has been used to
emulate the connection, so url-s, topics and such variables might need to be replaced.

The framework consist of 3 main parts:

## 1. EventEndPoint.java

This is a class that extends `WebSocketAdapter`. It houses a private `Session` object and a `JettyMessageListener` object.

Key Methods:
- `onWebSocketConnect`
- `onWebSocketText`
- `onWebSocketClose`

These methods are overrides from the WebSocketAdapter(Jetty) class. EventEndPoint.java also includes methods to set and get the message listener, and to await closure.

### onWebSocketConnect()
- handles the event in case a successful connection happens
- currently we get the Session object from here

### onWebSocketText()
- handles the event in case a message is being sent to a topic we are subscribed to
- JettyMessageListener will listen for incoming messages

### onWebSocketClose()
- logs the reason and status code of a websocket close. 


## 2. JettyTests.java

This is a Java class mainly containing tests for WebSocket connections using TestNg. The class has two example test :
one stomp test, and one standard websocket test. Assertions should be handled here.

### stompTest()
- It attempts to connect to a WebSocket
- Subscribes to a destination topic
- Sends a message ( which will be a stomp frame)
- Closes the connection

Note: a custom parser might be needed, in case the server sends back the whole frame as a message.

### simpleWebSocketTest()
- Connects to a standard WebSocket
- Sends a message
- Close the connection

## 3. MessageListener & StompFrameBuilder

### StompFrameBuilder

- helps the building of stomp frames with the help of activemq library
- under stompframes, there are several templates about how a stomp frame should look like.

Note: the stomframe examples might need to be changed to suit the current need of the project. They are just examples.

### JettyMessageListener

- implements a custom made MessageListener
- implemented to handle async processes and handle further operations with messages. 

